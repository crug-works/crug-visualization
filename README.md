# CRUG Visualization

![CRUG logo](crug_skyline.png)

## Repository for CRUG scripts and projects involving data visualizations across flavors, libraries, and stacks:

- ### Base R graphics
- ### D3 integration
- ### Geospatial and maps
- ### Plotly integration
- ### Tidyverse graphics

